from django.shortcuts import render
from django.http import HttpResponse
from .models import * 

# Create your views here.
def index(request): 

    #varEstudiante = Estudiante.objects.get(nombres = "Valeria", apellidos = "Enri")
    #print(varEstudiante)
    # Para obtener todos los estudiantes del grupo 1: 
    varEstudiante = Estudiante.objects.filter(grupo = 1) # .get cuando es único 
    varEstudiante4 = Estudiante.objects.filter(grupo = 4)

    # Ahora vamos a hacer una consulta para extraer los estudiantes que tengan la misma edad 
    # Primero vamos a hacerlo manual para que entienda como funciona paso a paso el filter 
    nueve = Estudiante.objects.filter(edad = 19)
    veinte = Estudiante.objects.filter(edad = 20)
    veinte1 = Estudiante.objects.filter(edad = 21)
    veinte2 = Estudiante.objects.filter(edad = 22)
    veinte3 = Estudiante.objects.filter(edad = 23)
    veinte4 = Estudiante.objects.filter(edad = 24)
    veinte5 = Estudiante.objects.filter(edad = 25)

    # All the estudiantes que sean del grupo 3 y que tengan la misma edad 
    grupo3_20 = Estudiante.objects.filter(grupo = 3, edad = 20)
    grupo3_21 = Estudiante.objects.filter(grupo = 3, edad = 21)
    grupo3_22 = Estudiante.objects.filter(grupo = 3, edad = 22)

    # Ya que entendí como funcionaba manual y que puedo aprovecharme de la funcionalidad de python, 
    # voy a extraer todos los apellidos únicos y después realizar las consultas 
    # Vamos a extraer a todos los estudiantes 
    estudiantes = Estudiante.objects.all()
    apellidos_unicos = set()
    for e in estudiantes: 
        apellidos_unicos.add(e.apellidos)
    
    res_apellidos = []
    for a in apellidos_unicos: 
        res_apellidos.append(Estudiante.objects.filter(apellidos = a))

    # return HttpResponse("Hola mundo")
    return render(request, 'index.html', {'varEstudiante':varEstudiante, "varEstudiante4":varEstudiante4, 
                                        "nueve": nueve, "veinte": veinte, "veinte1": veinte1, "veinte2": veinte2, 
                                        "veinte3": veinte3, "veinte4": veinte4, "veinte5": veinte5, 
                                        "estudiantes": estudiantes,  "grupo3_20": grupo3_20,
                                        "grupo3_21": grupo3_21, "grupo3_22": grupo3_22, "apellidos_unicos": res_apellidos})
